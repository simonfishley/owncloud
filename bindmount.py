#!/usr/bin/python
import ldap
import os
import subprocess
import uuid
import logging
from pwd import getpwnam

# Log activity to a file
logfile='/var/log/bindlog.log'
logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',filename=logfile,level=logging.DEBUG)

# Lookup all the users in the cloudcape group in ldap
path='dc=saao'
l=ldap.open('ldap1.cape.saao.ac.za')
l.protocol_version = ldap.VERSION3
l.simple_bind('dc=saao')
a=l.search_s(path,ldap.SCOPE_SUBTREE,'cn=cloudcape')

# Add members to uids
uids=a[0][1]['memberUid']

# Check if a bindmount already exists or it not, that each cloud group user has the required directory i.e.home/USER/cloud and /var/www/owncloud/data/USER/files
for uid in uids:
    # Define the directory paths
    cloudfiledir="/var/www/owncloud/data/"+uid+"/files"
    clouddir="/home/"+uid+"/cloud"
    try:
        subprocess.check_output(["mountpoint", cloudfiledir])
        # If the output is all good, their bind mount is setup and mounted, if not carry on
    except subprocess.CalledProcessError:
        print uid+" does not have a mountpoint"
    else:
        continue
    # Check if they have a /var/www/owncloud/data/UID folder
    if not os.path.isdir(cloudfiledir):
        print uid+" does not have a owncloud directory structure in /var/www/owncloud, creating it"
        os.makedirs("/var/www/owncloud/data/"+uid+"/files")
        os.makedirs("/var/www/owncloud/data/"+uid+"/cache")
        # Now set permissions ("+uid+":Domain Users) - os.chown(path, uid, gid) 
        os.chown("/var/www/owncloud/data/"+uid+"", 33, 33)
        os.chown("/var/www/owncloud/data/"+uid+"/files", 33, 33)
        os.chown("/var/www/owncloud/data/"+uid+"/cache/", 33, 33)
    # Now check if they have a /home/UID/cloud directory
    if not os.path.isdir(clouddir):
        # They don't have a cloud directory, creating one
        print uid+" does not have a cloud folder in their home directory, creating it"
        os.makedirs(clouddir)
        # Now set permissions ("+uid+":Domain Users) - os.chown(path, uid, gid)
        filt = "(uid=" + uid + ")"
        userid=l.search_s(path, ldap.SCOPE_SUBTREE, filt, ['uidNumber'])
        print "\t", uid, "filter: ", filt, userid,  "or"
        numuid = int(userid[0][1]['uidNumber'][0])
        os.chown(clouddir, numuid, 513)
    # Now make sure the cloudfilesdir is empty, if not make a backup so we don't overwrite data!
    try:
        if not os.listdir(cloudfiledir) == []:
            print "The mount point is not empty"
            os.rename(cloudfiledir, cloudfiledir + str(uuid.uuid4()))
            os.makedirs(cloudfiledir)
            os.chown(cloudfiledir, 33, 33)
    except:
        print "The mount point is empty, moving on"

    print "Now let's bind mount the directories"
    # Bind each users home dir to their cloud dir
    filt = "(uid=" + uid + ")"
    userid=l.search_s(path, ldap.SCOPE_SUBTREE, filt, ['uidNumber'])
    print "\t", uid, "filter: ", filt, userid,  "or"
    numuid = userid[0][1]['uidNumber'][0]
    print "\t\t", uid, filt, uid, numuid
    os.system('bindfs -M www-data --create-for-user=' + numuid + ' --create-for-group=513 ' + '/home/' + uid + '/cloud' + ' /var/www/owncloud/data/' + uid + '/files')
